const baseApiUrl = 'https://reqres.in';

//get 
var get = () => {

  // Make a request for a user with a given ID
  axios.get(baseApiUrl + '/api/users?page=2')
    .then(function (response) {
      // handle success
      console.log(response.data);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      // always executed
    });


  // Optionally the request above could also be done as
  axios.get(baseApiUrl + '/api/users', {
    params: {
      page: 2
    }
  })
    .then(function (response) {
      console.log(response.data);
    })
    .catch(function (error) {
      console.log(error);
    })
    .then(function () {
      // always executed
    });

}

//post
let post = () => {

  axios.post(baseApiUrl + '/api/users', {
    "name": "morpheus",
    "job": "leader"
  })
    .then(response => {
      console.log(response.data);
    })
    .catch(error => {
      console.log(error);
    })
    .then(console.log("worked"));
}

//multiple concurrent requests
let getUserAccounts = () => {
  return axios.get(baseApiUrl + '/api/users/2');
}

let getUserPermissions = () => {
  return axios.get(baseApiUrl + '/api/unknown');
}

let multipleConcurrentRequests = async () => {
  await Promise.all([getUserAccounts, getUserPermissions])
    .then(axios.spread(function (acct, perms) {
      // Both requests are now complete
      console.log(acct, perms);
    }));
}

//put 
let put = () => {
  axios.put(baseApiUrl + '/api/users/12', {
    "name": "morpheus",
    "job": "zion resident"
  })
    .then(response => console.log(response.data))
    .catch(error => console.log(error));
}

//patch
let patch = () => {
  axios.patch(baseApiUrl + '/api/users/21', {
    "name": "fgjsf",
    "job": "sdfsdf jksdjksd"
  })
    .then(response => console.log(response.data))
    .catch(error => console.log(error));
}

//delete
let deleteData = () => {
  axios.delete(baseApiUrl + '/api/users/4')
    .then(response => console.log(response))
    .catch(error => console.log(error));
}

//
get();
post();
multipleConcurrentRequests();
put();
patch();
deleteData();

